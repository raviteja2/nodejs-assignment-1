const http = require('http')
const fs = require('fs')

const server = http.createServer((req,res) =>{
    if(req.url === '/'){
        let rawData = fs.readFileSync('./text.json','utf8')
        let jsonData = JSON.parse(rawData)
        let data = JSON.stringify(jsonData)
        console.log(data)
        fs.writeFileSync('./data.txt',data, err =>{
            if(err){
                console.log("Error occured in writing a file data")
            }
        })
        res.writeHead(200,{'Content-Type':'text/html'})
        res.write(data)
        res.end()
    }
})

server.listen(8080,() =>{
    console.log("Server is listening")
})